/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

 //Import
import React  from 'react';
import { AppRegistry, Image, View, Text, TouchableOpacity, Alert } from 'react-native';

//Formatações
const Estilos={
    principal:{
        flex:1,
        justifyContent:'center',
        alignItems: 'center'
    },
    botao:{
        backgroundColor:'#538530',
        paddingVertical: 10,
        paddingHorizontal: 40,
        marginTop:20
    },
    textoBotao:{
        color:'white',
        fontSize:16,
        fontWeight:'bold'
    }
};

//Função de gerar frases
const gerarNovaFrase = () =>{
    //A função random gera numero aletorios, mas não há intervalo especifico
    var numAleatorio = Math.random();
    //Definindo o intervalo de numero aleatório
    //O método floor, elimina a parte francionária do numero aleatorio gerado, e cria somente inteiro
    numAleatorio = Math.floor( numAleatorio * 5);

    //Array de frases
    var frases = Array();
    frases[0] = 'Não deixe que as pessoas te façam desistir daquilo que você mais quer na vida. Acredite. Lute. Conquiste. E acima de tudo, seja feliz.';
    frases[1] = 'Não importa o que você decidiu. O que importa é que isso te faça feliz.';
    frases[2] = 'Algumas vezes coisas ruins acontecem em nossas vidas para nos colocar na direção das melhores coisas que poderíamos viver.';
    frases[3] = 'Se a vida não ficar mais fácil, trate de ficar mais forte.';
    frases[4] = 'Se a caminhada está difícil, é porque você está no caminho certo.';
    frases[5] = 'Toda conquista começa com a decisão de tentar.';

    //Variavel que recebe o numero aleatorio gerado para mostrar mensagem
    var fraseEscolhida = frases[numAleatorio];

    Alert.alert(fraseEscolhida);
}

//Criar o componente
const frasesDoDia = ()=>{
    return (
       <View style={Estilos.principal}>
            <Image source={require('./imgs/logo.png')}></Image>
            <TouchableOpacity 
            onPress={gerarNovaFrase}
            style={Estilos.botao}>
                <Text style={Estilos.textoBotao}>Nova Frase</Text>
            </TouchableOpacity>
       </View>
    )
}

//Renderizar para o dispositivo
AppRegistry.registerComponent('frasesDoDia', () => frasesDoDia);
